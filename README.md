# Projet Crawleth : Crawler pour le réseau Ethereum
  

## Installation des librairies python nécessaires
Le projet comporte un script d'installation des librairies `Python` nécessaires pour l'exécution du Crawler. <br>
Il est situé dans [./tools/linux_setup.sh](./tools/linux_setup.sh).<br>
Il est absolument nécessaire d'éxécuter au moins une fois ce script avant le lancement du crawler.

## Téléchargement / Mise à jour des bases de données
**Lors du démarrage du crawl, le crawler utilise un fichier contenant des noeuds de démarrage. Ce fichier doit être mis à jour régulièrement. <br> 
De plus, l'export du résulat du crawl nécessite des bases de données IP-Localisation/ASN à jour.** <br>
Le dépôt possède un script de récupération automatique de :
- du fichier de bootstrap nodes depuis [Geth](https://github.com/ethereum/go-ethereum).
- de la base de données IP-Localisation depuis [robcowar](https://github.com/robcowart/eslog_tutorial).
- de la base de données IP-ASN depuis [robcowar](https://github.com/robcowart/eslog_tutorial).

Le script peut se lancer de manière manuelle [./.githooks/pre-commit](./.githooks/pre-commit). <br>
Le githooks peut aussi être installé avec la command `make`. Il téléchargera automatiquement le fichier en cas de commit.<br>
**ATTENTION : fonctionne uniquement depuis le directory `Crawleth`**.

## Lancement du crawler
Le lancement du crawler nécessite au préalable le lancement en arriere plan de [./redis/launch_redis.sh](./redis/launch_redis.sh). <br>
Puis `python3 crawl.py`.

### Logger
Nous avons choisi de ne pas afficher systématiquement les actions du crawler. Celles-ci sont donc répertoriées dans un fichier de log : `last.log`.

### Options du crawler
Les options du crawler sont actuellement:

| Fonctionnalité                                                                                                             | Action                   |
| -------------------------------------------------------------------------------------------------------------------------- | ------------------------ |
| Lancement en effaçant le fichier de log                                                                                    |  `flush`                 |
| Charge la base de données Redis et l'exporte au format `json`                                                              |  `export`                |
| Charge la base de données Redis et l'exporte au format `json` en donnant pour nom de fichier la date de premier noeud `UP` |  `export_stats`          |         
| Après export de la base de données, analyse des données                                                                    |  `graphs`                |
| Lance le crawl en silence et exporte le résultat au format `json` avec pour nom de fichier la date de dernier noeud **up** |  `launch_silence`        |
| Affiche le nombre de noeuds connectés par continents par heure sur l'ensemble des fichiers exportés avec `export_stats`    |  `export_stats_graph`    |
| Affiche le % de noeuds quittant/(re)venant dans le réseau sur l'ensemble des fichiers exportés avec `export_stats`         |  `churn`                 | 

## Crawl sur un grand nombre de fois
L'objectif est de lancer le crawler plusieurs fois les unes à la suite des autres et de pouvoir avoir un état du réseau sur un certain nombre de crawls. <br>
A chaque crawl, le crawler va exporter ses résultats sous la forme d'un fichier `json` dans le dossier [stats](./stats). Le nom du fichier `json` est le temps du dernier noeud classé **up**. <br>
L'ensemble de fichiers `json` généré est ensuite analysable avec les commandes `export_stats_graph` et `churn`.

### Lancement
```
./automatic_launch.sh <number_of_crawls>
```
