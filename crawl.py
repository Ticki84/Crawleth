import sys
import logging

from bootnodes import MAINNET_BOOTNODES_GETH
from config import *
from p2p import ecies
from protocol import *

global timers, stopped


def add_boostrap_nodes(redis_db):
    for k in range(len(MAINNET_BOOTNODES_GETH)):
        remote, pubkey = from_enode_uri(MAINNET_BOOTNODES_GETH[k])
        remote_key: bytes = remote_to_key(remote)
        if redis_db.sismember(ADDED_POOL, remote_key) == 0:
            redis_db.sadd(ADDED_POOL, remote_key)
            redis_db.set(remote_key, pickle.dumps({'IP address': remote[0],
                                                   'UDP port': remote[1],
                                                   'TCP port': remote[2],
                                                   'Pending': formatted_date(),
                                                   'Ping': 0,
                                                   'Node ID': pubkey.to_hex(),
                                                   'Seen node IDs': {pubkey.to_hex()}}))
            redis_db.sadd(PING_PENDING_POOL, remote_key)


def check_node_timeout(redis_db, node_key):
    if redis_db.sismember(PING_WAITING_POOL, node_key):  # node hasn't responded yet, we'll retry
        redis_db.srem(PING_WAITING_POOL, node_key)
        node = pickle.loads(redis_db.get(node_key))
        if node['Ping'] < PING_ATTEMPT:
            redis_db.sadd(PING_PENDING_POOL, node_key)
        else:
            redis_db.sadd(DOWN_POOL, node_key)


def null_delta_check(conn, _null_delta, is_silence=False):
    global timers, stopped
    old = conn.redis_db.get(TOTAL_NODES)
    current = conn.redis_db.scard(ADDED_POOL)
    conn.redis_db.set(TOTAL_NODES, pickle.dumps(current))
    old = 0 if old is None else pickle.loads(old)
    conn.logger.info('{:d} new nodes discovered in {:.1f} seconds'.format(current - old, NULL_CHECK_INTERVAL))
    if conn.collect_neighbors:
        if current <= old + NULL_EPS:  # not enough new node were added
            _null_delta -= 1
            if _null_delta == 0:  # we don't need more nodes
                conn.collect_neighbors = False
                conn.logger.info('Stopped neighbors collect')
                return
        else:
            _null_delta = NULL_MAX  # we reset the count
    if conn.collect_neighbors and conn.redis_db.scard(PING_PENDING_POOL) == 0 and conn.redis_db.scard(
            PING_WAITING_POOL) == 0:
        # If the collect is done and pools are empty, the crawl is finished
        stopped = True
        for timer in timers:
            if timer.is_alive():
                timer.cancel()
        conn.logger.info('Pending and waiting pools are now empty')
        conn.close()
        export(conn.redis_db, conn.logger, is_silence)
        if not is_silence:
            generate_graphs()
        sys.exit(0)
    timer = threading.Timer(NULL_CHECK_INTERVAL, null_delta_check, args=[conn, _null_delta, is_silence])
    timers.append(timer)
    timer.start()


def process_pending(conn):
    """ Process the pending pool while new nodes are added to it
    """
    global timers, stopped
    nodes_key = conn.redis_db.smembers(PING_PENDING_POOL)
    to_pull = PING_MAX - conn.redis_db.scard(PING_WAITING_POOL)
    for node_key in nodes_key:
        if to_pull < 1 or stopped:
            break
        to_pull -= 1
        conn.redis_db.srem(PING_PENDING_POOL, node_key)
        node = pickle.loads(conn.redis_db.get(node_key))
        conn.ping([node['IP address'], node['UDP port'], node['TCP port']])
        if not stopped:
            timer = threading.Timer(PING_TIMEOUT, check_node_timeout, args=[conn.redis_db, node_key])
            timers.append(timer)
            timer.start()
    if not stopped:
        timer = threading.Timer(PING_CHECK_INTERVAL, process_pending, args=[conn])
        timers.append(timer)
        timer.start()


def main(argv):
    # Initialize logger
    TRACE = 9
    logging.addLevelName(TRACE, "TRACE")

    def trace(self, message, *args, **kws):
        if self.isEnabledFor(TRACE):
            self._log(TRACE, message, args, **kws)

    logging.Logger.trace = trace
    logformat = ("[%(process)d] %(asctime)s,%(msecs)05.1f %(levelname)s "
                 "(%(funcName)s) %(message)s")
    logging.basicConfig(level=logging.getLogger().setLevel(LOG_LEVEL),
                        format=logformat,
                        filename="last.log",
                        filemode='w' if 'flush' in argv else 'a')
    if('launch_silence' not in argv):
        print(("Log: {}, press CTRL+C to terminate..".format("last.log")))

    if 'export' in argv:
        redis_db = redis_init(flush=False)
        export(redis_db, logging.getLogger())
        exit(0)
    if 'export_stats' in argv:
        redis_db = redis_init(flush=False)
        export(redis_db, logging.getLogger(), True)
        exit(0)
    if 'export_stats_graph' in argv:
        if any(fname.endswith('.json') for fname in os.listdir(EXPORT_DIRECTORY)):
            plot_by_time_of_day()
        else:
            print("No .json file detected in " + EXPORT_DIRECTORY)
        exit(0)
    if 'graphs' in argv:
        generate_graphs()
        exit(0)
    if 'churn' in argv:
        plot_churn()
        exit(0)

    redis_db = redis_init()

    private_key = ecies.generate_privkey()
    conn = Connection(logger=logging.getLogger(),
                      private_key=private_key,
                      redis_db=redis_db)
    conn.open()

    add_boostrap_nodes(redis_db)

    global timers, stopped
    timers = []
    stopped = False
    null_delta_check(conn, NULL_MAX, 'launch_silence' in argv)
    process_pending(conn)

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
