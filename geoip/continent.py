# latitude, longitude
AMERICA = [[90, -167.856892], [-59.175928, -24.349205], 'AMERICA']

EUROPE = [[68.911005, -22.666353], [35.029996, 30.176383], 'EUROPE']

AFRICA = [[33.833920, -23.906250], [-37.055177, 52.031250], 'AFRICA']

ASIA = [[77.645947, 35.859375], [10.617418, 191.250000], 'ASIA']

OCEANIA = [[11.307708, 83.671875], [-54.085173, 197.578125], 'OCEANIA']

MIDDLE_EAST = [[43.675818, 26.718750], [13.368243, 66.875], 'MIDDLE_EAST']

CONTINENTS = [AMERICA, EUROPE, OCEANIA, MIDDLE_EAST, AFRICA, ASIA ] #dont change the order cause MIDDLE_EAST is included somehow in AFRICA area

continent_list = ['AMERICA', 'EUROPE', 'AFRICA', 'ASIA', 'OCEANIA', 'MIDDLE_EAST']
nb_of_continent = len(continent_list)

def whichContinent(latitude, longitude): #find which continent corresponds to a given latitude, longitude
	for continent in CONTINENTS:
		if(longitude < continent[1][1] and continent[0][1] < longitude ):
			if(latitude < continent[0][0] and continent[1][0] < latitude ):
				return continent[2]
	return 'UNCLASSIFIED'
