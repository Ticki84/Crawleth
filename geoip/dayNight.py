import ephem

def dayOrNight(latitude, longitude): # returns 0 if it's day time or 1 if night time
	user = ephem.Observer()
	user.lat = latitude    
	user.lon = longitude     
	user.elevation = 4        
	user.temp = 20            # current air temperature gathered manually
	user.pressure = 1019.5    # current air pressure gathered manually

	next_sunrise_datetime = user.next_rising(ephem.Sun()).datetime()
	next_sunset_datetime = user.next_setting(ephem.Sun()).datetime()
	it_is_day = next_sunset_datetime < next_sunrise_datetime
	it_is_night = next_sunrise_datetime < next_sunset_datetime
	if(it_is_day == True):
		return 0
	else:
		return 1