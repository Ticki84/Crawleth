import maxminddb
import json
import seaborn as sns
import matplotlib.pyplot as plt
from datetime import datetime
from geoip.hosting import HOSTING_ISP, COLLEGE_ISP
from geoip.dayNight import *
from geoip.continent import *
import numpy as np
import os
from statistics import *
from stats.stats_ import *

buckets = ['up', 'down', 'pending', 'waiting']
PERCENTILE_ = 0


def extract_country_from_ip(geoip, ip): #returns city and country given an IP
    try:
        res = geoip.get(ip)
        return [res['city']['names']['en'], res['country']['names']['en']]  # TODO add an option for changing language
    except (KeyError, TypeError):
        return None

def extract_dayNight_status(geoip, ip):
    try:
        res = geoip.get(ip)
        if(dayOrNight(str(res['location']['latitude']), str(res['location']['longitude'])) == 0):
            return 'Day'
        return 'Night'
    except (KeyError, TypeError, ephem.AlwaysUpError, ephem.NeverUpError):
        return None

def extract_ISP_from_ip(geoip, ip): #returns ISP given an IP
    try:
        res = geoip.get(ip)
        return res['autonomous_system_organization']
    except (KeyError, TypeError):
        return None

def extract_Continent_from_ISP(geoip, ip): #returns continent given an IP
    try:
        res = geoip.get(ip)
        continent = whichContinent(res['location']['latitude'], res['location']['longitude'])
        if(continent != 'UNCLASSIFIED'):
            return continent
        return None
    except (KeyError, TypeError, ephem.AlwaysUpError, ephem.NeverUpError):
        return None

def extract_info(arg, data):
    x = []
    y = []
    if (arg == 'country'):
        for i in data['up']:
            try:
                t = i['Node Country']
                if t not in x:
                    x.append(t)
                    y.append(1)
                else:
                    y[x.index(t)] += 1
            except (KeyError):
                pass
    if (arg == 'isp'):
        for i in data['up']:
            try:
                t = i['ISP']
                if t not in x:
                    x.append(t)
                    y.append(1)
                else:
                    y[x.index(t)] += 1
            except (KeyError):
                pass
    if (arg == 'isp_stats'):
        x = ['Hosting', 'College', 'Other']
        y = [0, 0, 0]
        for i in data['up']:
            try:
                t = i['ISP']
                if t in HOSTING_ISP:
                    y[0] += 1
                if t in COLLEGE_ISP:
                    y[1] += 1
                else:
                    y[2] += 1
            except (KeyError):
                pass
    if (arg == 'day/night'):
        x = ['Day', 'Night']
        y = [0, 0]
        for i in data['up']:
            try:
                t = i['Day/Night']
                if t == 'Day':
                    y[0] += 1
                else:
                    y[1] += 1
            except (KeyError):
                pass
    if (arg == 'day/night_moving'):
        x = [0]
        y = [[0,0]] #number of day, night nodes
        y_= [0]
        dict_ = {} #extracting up nodes and fulling a dictionnary
        for i in data['up']:
            try:
                dict_[i['Node ID']] = [i['Up'], i['Day/Night']]
            except (KeyError):
                pass
        number = 0;
        for k, v in sorted(dict_.items(), key=lambda x: x[1]): #sort the dictionnary by up time
            number +=1
            x.append(number)
            y_dernier = y[-1]
            a, b = y_dernier[0], y_dernier[1]
            if(v[1] == 'Night'):
                b += 1;
                y.append([a, b]) #updating number
            else:
                a += 1;
                y.append([a, b]) #updating number
            y_dernier = y[-1]
            y_.append((y_dernier[0]/(y_dernier[1] + y_dernier[0]))*100) #updating stats
        y = y_
    if (arg == 'up_time'):
        for i in data['up']:
            try:
                t = i['Up']
                if t not in x:
                    y.append(t)
            except (KeyError):
                pass
        x = range(0, len(y))
        y.sort()
        y_ = []
        begin = datetime.strptime(y[0], '%Y-%m-%d %H:%M:%S.%f')
        for k in y:
            y_.append((datetime.strptime(k, '%Y-%m-%d %H:%M:%S.%f') - begin).total_seconds())
        y = y_
    if (arg == 'continent'):
        x = ['AMERICA', 'EUROPE', 'AFRICA', 'ASIA', 'OCEANIA', 'MIDDLE_EAST']
        y = [0, 0, 0, 0, 0, 0]
        for i in data['up']:
            try:
                t = i['Continent']
                if t == 'AMERICA':
                    y[0] += 1
                if t == 'EUROPE':
                    y[1] += 1
                if t == 'AFRICA':
                    y[2] += 1
                if t == 'ASIA':
                    y[3] += 1
                if t == 'OCEANIA':
                    y[4] += 1
                if t == 'MIDDLE_EAST':
                    y[5] += 1
            except (KeyError):
                pass

    if (arg == 'pending'): #nodes in pending bucket
        for j in buckets:
            for i in data[j]:
                try:
                    t = i['Pending']
                    if t not in x:
                        y.append(t)
                except (KeyError):
                    pass

        x = range(0, len(y))
        y.sort()
        y_ = []
        begin = datetime.strptime(y[0], '%Y-%m-%d %H:%M:%S.%f')
        for k in y:
            y_.append((datetime.strptime(k, '%Y-%m-%d %H:%M:%S.%f') - begin).total_seconds())
        y = y_
    return x, y



def print_country_data(x, y, title=None):
    plt.subplots_adjust(left=0.15, bottom=0, right=1, top=1, wspace=0, hspace=0)
    sns.set_context('paper')
    tips = sns.load_dataset('tips')
    sns.barplot(x=y, y=x, orient='h')
    if title:
        plt.title(title)
    plt.show()

def pie(x, y,  percentile, title=None):
    if(percentile != 0):
        x_, y_ = cut_to_quartile(x, y, percentile)
    else:
        x_, y_ = x, y
    plt.pie(x_, labels = y_, normalize = True)
    if title:
        if percentile != 0:
            title += " (" + str(100 - percentile) + "% most represented)"
        plt.title(title)
    plt.show()

def print_time_data(x, y, title=None, xlabel=None, ylabel=None):
    sns.set()
    plt.plot(x, y)
    if(xlabel == None):
        plt.xlabel('Time (s)')
    else:
        plt.xlabel(xlabel)
    if(ylabel == None):
        plt.ylabel('Number of nodes')
    else:
        plt.ylabel(ylabel)
    
    if title:
        plt.title(title)
    plt.show()

def cut_to_quartile(x, y, percentile_):
	Q = round(np.percentile(x, percentile_))
	x_ = []
	y_ = []
	n = len(x)
	for k in range(n):
		if(x[k] > Q):
			x_.append( x[k] )
			y_.append( y[k] )
	return x_, y_

def plot_by_time_of_day():
    x_ = []
    y = [[], [], [], [], [], []]

    for nom in os.listdir(EXPORT_DIRECTORY) : #for each file in EXPORT_DIRECTORY
        if(nom not in NOT_JSON_NAMES): #check if it is a json file
            try :
                ofi=open(EXPORT_DIRECTORY+"/"+nom,'r')
                data = json.load(ofi) #find the datas in the json file
                ofi.close()
                x, y_ = extract_info('continent', data)

                #find the exact point for time axis
                heure_ = int(nom.split(' ')[1].split(':')[0]) + 2 - 1
                if heure_ == 24:
                    heure_ = 0
                minute_ = int(nom.split(' ')[1].split(':')[1])
                seconds_ = int(nom.split(' ')[1].split('.')[0].split(':')[2])
                m_seconds_ = int(nom.split(' ')[1].split('.')[1])
                point = heure_ + minute_/60 + m_seconds_/1000
                if(y_ != [0] * nb_of_continent): #no geoip db present when crawl was achieved
                    if point in x: #point already exists, pretty rare but it may happen
                        index_ = x_.index(point)
                        for k in range(nb_of_continent):
                            y__ = (y[k][index_] + y_[k]) / 2 #pretty bad way to reckon mean but it's so rare that it's ok
                            y[k][index_] = y__
                    else :
                        x_.append(point)
                        index_ = x_.index(point)
                        x_.sort()
                        for k in range(nb_of_continent):
                            y__ = y[k]
                            y__.insert(index_, y_[k])
                            y[k] = y__
            except(IsADirectoryError):
                pass;
    for k in range(nb_of_continent):
        plt.plot(x_, y[k], label = continent_list[k])
    plt.xlabel('Time (h)')
    plt.ylabel('Number of nodes')
    plt.legend()
    plt.show()

def plot_churn(): #displays percentage of nodes leaving/(re)joining the network
    x_ = []
    entering = []
    leaving = []
    names = []

    data_before = None
    for nom in sorted(os.listdir(EXPORT_DIRECTORY)) : #for each file in EXPORT_DIRECTORY
        if(nom not in NOT_JSON_NAMES): #check if it is a json file
            if data_before != None: #from the second .json file to the end
                names.append(nom.split('.json')[0].split('.')[0]) #extract time from file name
                try :
                    ofi=open(EXPORT_DIRECTORY+"/"+nom,'r')
                    data = json.load(ofi) #find the datas in the json file
                    ofi.close()

                    data = data['up'] #only processes up nodes
                    data_ = []
                    for k in range(len(data)):
                        pending_data = data[k]
                        data_.append([pending_data['IP address'], pending_data['UDP port']]) #each node is unicaly identified by its IP-UDP port cuple

                    #reckons percentage of nodes entering the network
                    diff = 0;
                    longueur_data_before = len(data_before)
                    longueur_data_ = len(data_)

                    for k in range(len(data_)):
                        elt = data_[k]
                        isIn = False;
                        for i in range(longueur_data_before):
                            if elt == data_before[i]:
                                isIn = True;
                                break;
                        if isIn == False: #not seen
                            diff+=1

                    entering.append( (diff / (longueur_data_) ) *100  )

                    #reckons percentage of nodes leaving the network
                    diff = 0;
                    for k in range(len(data_before)):
                        elt = data_before[k]
                        isIn = False;
                        for i in range( longueur_data_ ):
                            if elt == data_[i]:
                                isIn = True;
                                break;
                        if isIn == False: #not seen
                            diff+=1

                    leaving.append( (diff / (longueur_data_before) ) *100  )

                    timestamp = datetime.timestamp( datetime.strptime(nom.split('.json')[0].split('.')[0], '%Y-%m-%d %H:%M:%S')  )

                    x_.append(timestamp)
                    data_before = data_ #now the json file content is said to be old to be compared with next one
                    
                except(IsADirectoryError):
                    pass;
            else: #first one
                ofi=open(EXPORT_DIRECTORY+"/"+nom,'r')
                data = json.load(ofi) #find the datas in the json file
                ofi.close()
                data = data['up']
                data_ = []
                for k in range(len(data)):
                    pending_data = data[k]
                    data_.append([pending_data['IP address'], pending_data['UDP port']])
                data_before = data_
    sns.set()
    plt.plot(x_, leaving, label='Nodes leaving the network')
    plt.plot(x_, entering, label='Nodes (re)joining the network')

    #finding y limit for display
    max_y = 0
    if (max(leaving) + 15 > 100) or ( max(entering) + 15 > 100): #one add 15% to find the limit if it's lower than 100%
        max_y = 100
    else:
        max_y = max(max(leaving) + 15, max(entering) + 15)
    plt.axis([x_[0], x_[-1], 0, max_y])

    plt.xticks(x_, names, rotation=45)
    plt.xlabel('Time (h)')
    plt.ylabel('Rate (%)')
    plt.title('Percentage of changes')
    plt.subplots_adjust(left=0.125, bottom=0.32, right=0.9, top=0.88, wspace=0.2, hspace=0.2)
    plt.legend()
    plt.show()

    
    



def generate_graphs(): #in charge of generating graphs after crawl
    f = open('export.json', )
    data = json.load(f)
    f.close()

    x, y = extract_info('pending', data)
    if x != [] and y != []:
        print_time_data(y, x, 'Total of discovered nodes in function of the elapsed time')

    x, y = extract_info('up_time', data)
    if x != [] and y != []:
        print_time_data(y, x, 'Total of up nodes in function of the elapsed time')


    # WARNING: requires location/ISP db:

    x, y = extract_info('country', data)
    if x != [] and y != []:
        print_country_data(x, y, 'Geographical repartition of nodes')
        pie(y, x, PERCENTILE_, 'Geographical repartition of nodes')

    x, y = extract_info('isp', data)
    if x != [] and y != []:
    	pie(y, x, PERCENTILE_, 'ISP')

    x, y = extract_info('isp_stats', data)
    if (x != [] and y != []) and (y != [0, 0, 0]):
        pie(y, x, 0, 'ISP proportion')

    x, y = extract_info('day/night', data)
    if (x != [] and y != []) and (y != [0, 0]) :
        pie(y, x, 0, 'Day/Night distribution')

    x, y = extract_info('day/night_moving', data)
    if (x != [] and y != []) and (y != [0]):
        print_time_data(x, y, 'Percentage of nodes in day time zones in function of the number of up nodes classified', "Number of nodes", "Percentage")

    x, y = extract_info('continent', data)
    if (x != [] and y != []) and (y != [0, 0, 0, 0, 0, 0]):
        pie(y, x, 0, 'Continent distribution')   