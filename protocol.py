import ipaddress
import logging
import secrets
import socket
import struct
from datetime import datetime
from typing import Any, Sequence, Tuple, List
import re
import threading
import gevent
from redis_ import *

from urllib import parse as urlparse
import eth_keys
import rlp
from eth_keys import datatypes, keys
from eth_typing import Hash32
from eth_utils import to_bytes, keccak, encode_hex, decode_hex
import time
import pickle
from config import FROM_UDP_PORT, FROM_TCP_PORT

# Max size of discovery packets.
DISCOVERY_MAX_PACKET_SIZE = 1280

# Buffer size used for incoming discovery UDP datagrams (must be larger than
# DISCOVERY_MAX_PACKET_SIZE)
DISCOVERY_DATAGRAM_BUFFER_SIZE = DISCOVERY_MAX_PACKET_SIZE * 2

KADEMLIA_PUBLIC_KEY_SIZE = 512
# UDP packet constants.
MAC_SIZE = 256 // 8  # 32
SIG_SIZE = 520 // 8  # 65
HEAD_SIZE = MAC_SIZE + SIG_SIZE  # 97
EXPIRATION = 60  # let messages expire after N secondes
PROTOCOL_VERSION = 4

CMD_PING = 1
CMD_PONG = 2
CMD_FIND_NODE = 3
CMD_NEIGHBOURS = 4


def int_to_big_endian4(value: int) -> bytes:
    """ 4 bytes big endian integer"""
    return struct.pack('>I', value)


def enc_port(p: int) -> bytes:
    return int_to_big_endian4(p)[-2:]


def int_to_big_endian(value: int) -> bytes:
    return value.to_bytes((value.bit_length() + 7) // 8 or 1, "big")


def big_endian_to_int(value: bytes) -> int:
    return int.from_bytes(value, "big")


def remote_to_str(remote: Tuple[str, int, int]) -> str:
    return remote[0] + ':' + str(remote[1])


def _pack_v4(cmd_id: int, payload: Sequence[Any], privkey) -> bytes:
    """Create and sign a UDP message to be sent to a remote node.

    See https://github.com/ethereum/devp2p/blob/master/rlpx.md#node-discovery for information on
    how UDP packets are structured.
    """
    cmd_id_bytes = to_bytes(cmd_id)
    encoded_data = cmd_id_bytes + rlp.encode(payload)
    signature = privkey.sign_msg(encoded_data)
    message_hash = keccak(signature.to_bytes() + encoded_data)
    return message_hash + signature.to_bytes() + encoded_data


def _unpack_v4(message: bytes) -> Tuple[datatypes.PublicKey, int, Tuple[Any, ...], Hash32]:
    """Unpack a discovery v4 UDP message received from a remote node.

    Returns the public key used to sign the message, the cmd ID, payload and hash.
    """
    message_hash = Hash32(message[:MAC_SIZE])
    if message_hash != keccak(message[MAC_SIZE:]):
        raise SyntaxError("Wrong msg mac")
    signature = eth_keys.keys.Signature(message[MAC_SIZE:HEAD_SIZE])
    signed_data = message[HEAD_SIZE:]
    remote_pubkey = signature.recover_public_key_from_msg(signed_data)
    cmd_id = message[HEAD_SIZE]
    payload = tuple(rlp.decode(message[HEAD_SIZE + 1:], strict=False))
    return remote_pubkey, cmd_id, payload, message_hash


def check_relayed_addr(sender: ipaddress, addr: ipaddress) -> bool:
    """Check if an address relayed by the given sender is valid.

    Reserved and unspecified addresses are always invalid.
    Private addresses are valid if the sender is a private host.
    Loopback addresses are valid if the sender is a loopback host.
    All other addresses are valid.
    """
    if addr.is_unspecified or addr.is_reserved:
        return False
    if addr.is_private and not sender.is_private:
        return False
    if addr.is_loopback and not sender.is_loopback:
        return False
    return True


def _extract_nodes_from_payload(
        sender: Tuple[str, int, int],
        payload: List[Tuple[str, bytes, bytes, bytes]],
        logger: logging) -> List[Tuple[Tuple[str, int, int], keys.PublicKey]]:
    res = []
    sender_ip = ipaddress.ip_address(sender[0])
    for item in payload:
        ip, udp_port, tcp_port, node_id = item
        addr_ip = ipaddress.ip_address(ip)
        if check_relayed_addr(sender_ip, addr_ip):
            res.append(((addr_ip.exploded, big_endian_to_int(udp_port), big_endian_to_int(tcp_port)),
                        eth_keys.keys.PublicKey(node_id)))
        else:
            logger.debug("Skipping invalid address %s relayed by %s", ip, sender[0])
    return res


def validate_enode_uri(enode: str, require_ip: bool = False, logger: logging = logging) -> bool:
    try:
        parsed = urlparse.urlparse(enode)
    except ValueError as e:
        return False

    if parsed.scheme != 'enode' or not parsed.username:
        logger.warning('enode string must be of the form "enode://public-key@ip:port" for %s', enode)
        return False

    if not re.match('^[0-9a-fA-F]{128}$', parsed.username):
        logger.warning('Public key must be a 128-character hex string for %s', enode)
        return False

    decoded_username = decode_hex(parsed.username)

    try:
        ip = ipaddress.ip_address(parsed.hostname)
    except ValueError as e:
        logger.warning(str(e))
        return False

    if require_ip and ip in (ipaddress.ip_address('0.0.0.0'), ipaddress.ip_address('::')):
        logger.warning('A concrete IP address must be specified for %s', enode)
        return False

    keys.PublicKey(decoded_username)

    try:
        # this property performs a check that the port is in range
        parsed.port
    except ValueError as e:
        logger.warning(str(e))
        return False


def from_enode_uri(uri: str, logger: logging = logging) -> Tuple[Tuple[str, int, int], datatypes.PublicKey]:
    validate_enode_uri(uri, False, logger)  # Be no more permissive than the validation
    parsed = urlparse.urlparse(uri)
    pubkey = keys.PublicKey(decode_hex(parsed.username))
    return (parsed.hostname, parsed.port, parsed.port), pubkey


def random_lookup() -> bytes:
    """ Generate a random node public key to lookup.
    """
    return int_to_big_endian(
        secrets.randbits(KADEMLIA_PUBLIC_KEY_SIZE)
    ).rjust(KADEMLIA_PUBLIC_KEY_SIZE // 8, b'\x00')


def formatted_date() -> str:
    return datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]


class Connection(object):
    def __init__(self, from_addr=("127.0.0.1", FROM_UDP_PORT, FROM_TCP_PORT),
                 **conf):
        self.from_addr: Tuple[str, int, int] = from_addr
        self.logger = conf.get('logger', logging)
        self.expiration: int = conf.get('expiration', EXPIRATION)
        self.private_key: datatypes.PrivateKey = conf.get('private_key', None)
        self.socket: socket = None
        self.serve_thread = None
        self.redis_db = conf.get('redis_db', redis_init())
        self.collect_neighbors = True

    def open(self):
        self.socket = socket.socket(socket.AF_INET,
                                    socket.SOCK_DGRAM)
        self.socket.settimeout(self.expiration)
        self.socket.bind(('', self.from_addr[1]))
        self.logger.info("Socket opened and binded on port %d", self.from_addr[1])
        self.serve_thread = threading.Thread(target=self.serve_forever)
        # self.serve_thread.daemon = True
        self.serve_thread.start()

    def close(self):
        if self.socket:
            sock = self.socket
            self.socket = None
            try:
                sock.shutdown(socket.SHUT_RDWR)
                self.logger.info("Socket closed")
            except socket.error:
                pass
            finally:
                sock.close()

    def send(self, remote: Tuple[str, int, int], cmd_id: int, payload: Sequence[Any]):
        """
            Pack the given payload using the given msg type and send it over our socket.

            If we get an OSError from our socket when attempting to send it, that will be logged
            and the message will be lost.
            """
        message = _pack_v4(cmd_id, payload, self.private_key)
        try:
            self.socket.sendto(message, (remote[0], remote[1]))
        except Exception as e:
            if self.socket:
                self.logger.error("Unexpected error when sending msg to %s:", remote)
                self.logger.error(e)
        return message

    def _get_msg_expiration(self):
        return rlp.sedes.big_endian_int.serialize(int(time.time() + self.expiration))

    def _is_msg_expired(self, rlp_expiration: bytes) -> bool:
        expiration = rlp.sedes.big_endian_int.deserialize(rlp_expiration)
        if time.time() > expiration:
            self.logger.warning('Received message already expired')
            return True
        return False

    def _find_node(self, remote: Tuple[str, int, int], target_key: bytes) -> None:
        remote_key = remote_to_key(remote)
        if not self.redis_db.sismember(ASKED_POOL, remote_key) and self.collect_neighbors:
            # If we haven't get a FindNode response yet from node
            self.redis_db.sadd(ASKED_POOL, remote_key)
            self.send_find_node_v4(remote, target_key)
            # self.send_find_node_v4(remote, random_lookup())

    def ping(self, remote: Tuple[str, int, int]):
        version = rlp.sedes.big_endian_int.serialize(PROTOCOL_VERSION)
        expiration = self._get_msg_expiration()
        this_address = [ipaddress.ip_address(self.from_addr[0]).packed, enc_port(self.from_addr[1]),
                        enc_port(self.from_addr[2])]
        node_address = [ipaddress.ip_address(remote[0]).packed, enc_port(remote[1]), enc_port(remote[2])]
        payload = (version, this_address, node_address, expiration)
        message = self.send(remote, CMD_PING, payload)
        # Return the msg hash, which is used as a token to identify pongs.
        token = Hash32(message[:MAC_SIZE])

        remote_key = remote_to_key(remote)
        node = pickle.loads(self.redis_db.get(remote_key))
        node['Ping'] += 1
        node['Ping ' + str(node['Ping'])] = formatted_date()
        self.redis_db.set(remote_key, pickle.dumps(node))
        self.redis_db.set(pickle.dumps(encode_hex(token)), remote_key)
        self.redis_db.sadd(PING_WAITING_POOL, remote_key)
        self.logger.debug('>>> ping (v4) %s (token == %s)', remote, encode_hex(token))

        return token

    def send_pong_v4(self, remote: Tuple[str, int, int], token: Hash32) -> None:
        expiration = self._get_msg_expiration()
        self.logger.debug('>>> pong %s', remote)
        node_address = [ipaddress.ip_address(remote[0]).packed, enc_port(remote[1]),
                        enc_port(remote[2])]
        payload = (node_address, token, expiration)
        self.send(remote, CMD_PONG, payload)

    def send_find_node_v4(self, remote: Tuple[str, int, int], target_key: bytes) -> None:
        if len(target_key) != KADEMLIA_PUBLIC_KEY_SIZE // 8:
            raise ValueError(f"Invalid FIND_NODE target ({target_key!r}). Length is not 64")
        expiration = self._get_msg_expiration()
        self.logger.debug('>>> find_node to %s', remote)
        self.send(remote, CMD_FIND_NODE, (target_key, expiration))

    def serve_forever(self) -> None:
        self.logger.info("Serving...")
        while self.socket:
            try:
                datagram, (ip_address, port) = self.socket.recvfrom(DISCOVERY_DATAGRAM_BUFFER_SIZE)
                gevent.spawn(self.consume_datagram((ip_address, port, port), datagram))
            except TimeoutError:
                self.close()
                exit(0)
            except ConnectionResetError as e:
                self.logger.warning(e)
            except Exception as e:
                if self.socket:
                    self.logger.error(e)

    def consume_datagram(self, address: Tuple[str, int, int], datagram):
        self.logger.trace("Received datagram from %s", address)
        self.handle_msg(address, datagram)

    def handle_msg(self, address: Tuple[str, int, int], message: bytes) -> None:
        try:
            remote_pubkey, cmd_id, payload, message_hash = _unpack_v4(message)
        except SyntaxError as e:
            self.logger.error('Error unpacking message (%s) from %s: %s', message, address, e)
            return

        remote_key = remote_to_key(address)
        if self.redis_db.exists(remote_key):
            node = pickle.loads(self.redis_db.get(remote_key))
            if node.get('Node ID'):
                node['Seen node IDs'].add(node['Node ID'])
            node['Node ID'] = remote_pubkey.to_hex()
            self.redis_db.set(remote_key, pickle.dumps(node))

        self.logger.trace("Received cmd %s from %s with payload: %s", cmd_id, address, payload)
        if cmd_id == CMD_PING:
            return self.recv_ping_v4(address, remote_pubkey, payload, message_hash)
        elif cmd_id == CMD_PONG:
            return self.recv_pong_v4(address, remote_pubkey, payload, message_hash)
        elif cmd_id == CMD_FIND_NODE:
            return
        elif cmd_id == CMD_NEIGHBOURS:
            return self.recv_neighbours_v4(address, payload, message_hash)
        else:
            raise ValueError(f"Unknown command id: {cmd_id}")

    def recv_ping_v4(
            self, remote: Tuple[str, int, int], remote_pubkey: keys.PublicKey, payload: Sequence[Any],
            message_hash: Hash32) -> None:
        """Process a received ping packet.

        A ping packet may come any time, unrequested, or may be prompted by us bond()ing with a
        new node. In the former case we'll just reply with a pong, whereas in the latter we'll
        also send an empty msg on the appropriate channel from ping_channels, to notify any
        coroutine waiting for that ping.

        Also, if we have no valid bond with the given remote, we'll trigger one in the background.
        """
        # The ping payload should have at least 4 elements: [version, from, to, expiration], with
        # an optional 5th element for the node's ENR sequence number.
        if len(payload) < 4:
            self.logger.warning('Ignoring PING msg with invalid payload: %s', payload)
            return
        elif len(payload) == 4:
            _, _, _, expiration = payload[:4]
            enr_seq = None
        else:
            _, _, _, expiration, enr_seq = payload[:5]
            enr_seq = big_endian_to_int(enr_seq)
        self.logger.debug('<<< ping(v4) from %s, enr_seq=%s', remote, enr_seq)
        self._is_msg_expired(expiration)

        remote_key = remote_to_key(remote)
        if not self.redis_db.sismember(ADDED_POOL, remote_key):
            # If a node sent a ping before we found it
            self.redis_db.sadd(ADDED_POOL, remote_key)
            self.redis_db.set(remote_key, pickle.dumps({'IP address': remote[0],
                                                        'UDP port': remote[1],
                                                        'TCP port': remote[2],
                                                        'Pending': formatted_date(),
                                                        'Ping': 0,
                                                        'Parents': {remote_to_str(remote)},
                                                        'Node ID': remote_pubkey.to_hex(),
                                                        'Seen node IDs': {remote_pubkey.to_hex()}}))
            self.redis_db.sadd(PING_PENDING_POOL, remote_key)
        else:
            self.send_pong_v4(remote, message_hash)
            self._find_node(remote, remote_pubkey.to_bytes())

    def recv_pong_v4(self, remote: Tuple[str, int, int], remote_pubkey: keys.PublicKey, payload: Sequence[Any],
                     _: Hash32) -> None:
        # The pong payload should have at least 3 elements: to, token, expiration
        if len(payload) < 3:
            self.logger.warning('Ignoring PONG msg with invalid payload: %s', payload)
            return
        elif len(payload) == 3:
            _, token, expiration = payload[:3]
            enr_seq = None
        else:
            _, token, expiration, enr_seq = payload[:4]
            enr_seq = big_endian_to_int(enr_seq)
        self._is_msg_expired(expiration)

        remote_key: bytes = self.redis_db.get(pickle.dumps(encode_hex(token)))
        self.redis_db.srem(PING_WAITING_POOL, remote_key)
        self.redis_db.sadd(UP_POOL, remote_key)
        node = pickle.loads(self.redis_db.get(remote_key))
        node['Up'] = formatted_date()
        self.redis_db.set(remote_key, pickle.dumps(node))
        self.logger.debug('<<< pong (v4) from %s (token == %s)', remote, encode_hex(token))

        # Send FindNode if the node haven't send a Ping back
        threading.Timer(0.05, self._find_node, args=[remote, remote_pubkey.to_bytes()]).start()

    def recv_neighbours_v4(self, remote: Tuple[str, int, int], payload: Sequence[Any], _: Hash32) -> None:
        # The neighbours payload should have 2 elements: nodes, expiration
        if len(payload) < 2:
            self.logger.warning('Ignoring NEIGHBOURS msg with invalid payload: %s', payload)
            return
        nodes, expiration = payload[:2]
        self._is_msg_expired(expiration)
        try:
            neighbours = _extract_nodes_from_payload(remote, nodes, self.logger)
        except ValueError:
            self.logger.warning("Malformed NEIGHBOURS packet from %s: %s", remote, nodes)
            return

        self.redis_db.sadd(COMPLETED_POOL, remote_to_key(remote))
        for neighbour in neighbours:
            neighbour_key: bytes = remote_to_key(neighbour[0])
            if not self.redis_db.sismember(ADDED_POOL, neighbour_key):
                self.redis_db.sadd(ADDED_POOL, neighbour_key)
                self.redis_db.set(neighbour_key, pickle.dumps({'IP address': neighbour[0][0],
                                                               'UDP port': neighbour[0][1],
                                                               'TCP port': neighbour[0][2],
                                                               'Pending': formatted_date(),
                                                               'Ping': 0,
                                                               'Parents': {remote_to_str(remote)},
                                                               'Seen node IDs': {neighbour[1].to_hex()}}))
                self.redis_db.sadd(PING_PENDING_POOL, neighbour_key)
            else:
                node = pickle.loads(self.redis_db.get(neighbour_key))
                if node.get('Parents'):
                    node['Parents'].add(remote_to_str(remote))
                node['Seen node IDs'].add(neighbour[1].to_hex())
                self.redis_db.set(neighbour_key, pickle.dumps(node))
        self.logger.debug('<<< %s neighbours from %s: %s', len(neighbours), remote, neighbours)
