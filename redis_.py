from eth_utils import encode_hex

import redis
import pickle
from parse_data import *
from stats.stats_ import *
from typing import Tuple
from config import *


def redis_init(flush=True):
    redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
    if flush:
        redis_db.flushdb()
    return redis_db


def remote_to_key(remote: Tuple[str, int, int]) -> bytes:
    return pickle.dumps(remote[0] + '_' + str(remote[1]))


def export(redis_db, logger, date_name=False):  # export redis database to json
    logger.info(str(redis_db.scard(ADDED_POOL)) + " nodes added")
    logger.info(str(redis_db.scard(COMPLETED_POOL)) + " nodes fully crawled out of " + str(redis_db.scard(ASKED_POOL)) +
                " asked nodes. If the difference is too high, consider reducing (NO)CRAWL_MAX_NODES_TO_WAIT")
    geoip = None
    geoip2 = None
    try:
        geoip = maxminddb.open_database('geoip/GeoLite2-City.mmdb')
    except FileNotFoundError:
        pass
    try:
        geoip2 = maxminddb.open_database('geoip/GeoLite2-ASN.mmdb')
    except FileNotFoundError:
        pass
    data = {}
    for pool in EXPORT_POOLS:
        data[pool] = []
        nodes_key = redis_db.smembers(pool)
        for node_key in nodes_key:
            if redis_db.exists(node_key):
                node = pickle.loads(redis_db.get(node_key))
                if node.get('Parents'):  # we don't export bootstrap nodes
                    node['Parents'] = list(node['Parents'])
                    node['Seen node IDs'] = list(node['Seen node IDs'])
                    if geoip: #add location infos
                        location = extract_country_from_ip(geoip, node['IP address'])
                        if location:
                            node['Node City'] = location[0]
                            node['Node Country'] = location[1]
                        dayNightStatus = extract_dayNight_status(geoip, node['IP address'])
                        if dayNightStatus:
                            node['Day/Night'] = dayNightStatus
                        continent = extract_Continent_from_ISP(geoip, node['IP address'])
                        if continent:
                            node['Continent'] = continent
                    if geoip2: #add ISPs infos
                        location = extract_ISP_from_ip(geoip2, node['IP address'])
                        if location:
                            node['ISP'] = location
                    data[pool].append(node)
        logger.info("Exported " + str(len(nodes_key)) + " entries for " + pool)
    if (date_name == True):  # export file name corresponds to last found node classified as UP
        dict_ = {}
        for i in data['up']:
            try:
                dict_[i['Node ID']] = [i['Up']]
            except (KeyError):
                pass
        with open(EXPORT_DIRECTORY + sorted(dict_.items(), key=lambda x: x[1])[-1][1][0] + '.json', 'w') as outfile:
            json.dump(data, outfile, indent=2)
    else:
        with open(EXPORT_NAME, 'w') as outfile:
            json.dump(data, outfile, indent=2)
