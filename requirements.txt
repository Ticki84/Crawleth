rlp~=2.0.1
cryptography~=3.1.1
trio~=0.16.0
lahja~=0.17.0
netifaces~=0.10.9
pyformance~=0.4
gevent~=20.9.0
redis~=3.5.3
requests~=2.24.0