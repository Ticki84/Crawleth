EXPORT_DIRECTORY = 'stats/'
from stats.stats_ import *

NOT_JSON_NAMES = [
		'time_repartion_AFRICA.png', 
		'time_repartion_AMERICA.png',
 		'time_repartion_ASIA.png',
  		'time_repartion_EUROPE.png',
   		'time_repartion_MIDDLE_EAST.png',
    	'time_repartion_OCEANIA.png',
    	'stats_.py',
    	'__pycache__']