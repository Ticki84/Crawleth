sudo apt-get -y install python3-pip
pip3 install eth-hash[pycryptodome]
pip3 install rlp
pip3 install gevent
pip3 install eth_keys
pip3 install eth-utils
echo "Uninstalling existing redis version"
python3 -m pip uninstall redis
pip3 install redis==2.10.6

sudo apt install redis-server
pip3 install python-geoip-geolite2
pip3 install python-geoip-python3
pip3 install seaborn
pip3 install ephem
pip3 install maxminddb
